#include "raylib.h"
#include <complex>

typedef struct {
    float x;
    float y;
} Point;

typedef struct {
    int x;
    int y;
} Pixel;

typedef struct {
    const int width;
    const int height;
} Screen;
Screen SCREEN = {1000, 1000};
const int MAX_ITERATION = 50;

void init() {
    InitWindow(SCREEN.width, SCREEN.height, "Kucing");
    SetTargetFPS(60);
}

bool isInSet(Point point, int& iterations) {
    std::complex<float> z(0, 0);
    std::complex<float> c(point.x, point.y);
    float boundary_radius = 2.0f;
    iterations = 0;
    while (std::abs(z) <= boundary_radius && iterations < MAX_ITERATION) {
        z = z * z + c;
        iterations++;
    }
    return iterations == MAX_ITERATION;
}

Point toPlotCoordinate(Pixel pixel) {
    const float scale = 3000.0f;
    return {((float)pixel.x - (float)SCREEN.width / 2) / scale, ((float)pixel.y - 3 * (float)SCREEN.height) / scale};
}

Color colorMap(int iterations) {
    if (iterations == MAX_ITERATION) {
        return BLACK;
    }

    float t = (float)iterations / (float)MAX_ITERATION;

    auto r = (unsigned char)(9 * (1 - t) * t * t * t * 255);
    auto g = (unsigned char)(15 * (1 - t) * (1 - t) * t * t * 255);
    auto b = (unsigned char)(8.5 * (1 - t) * (1 - t) * (1 - t) * t * 255);

    return Color{r, g, b, 255};
}

void draw() {
    for (int x = 0; x < SCREEN.width; x++) {
        for (int y = 0; y < SCREEN.height; y++) {
            Point point = toPlotCoordinate({x, y});
            int iterations;
            if (isInSet(point, iterations)) {
                DrawPixel(x, y, BLACK);
            } else {
                DrawPixel(x, y, colorMap(iterations));
            }
        }
    }
}

int main() {
    init();

    bool isDrawn = false;
    while (!WindowShouldClose()) {
        BeginDrawing();
        if (!isDrawn) {
            ClearBackground(BLACK);
            draw();
            isDrawn = true;
        }
        EndDrawing();
    }

    CloseWindow();
    return 0;
}
